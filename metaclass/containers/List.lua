local class = require 'metaclass'
local List = class "List" : noautocast()

local methods = {}

local function serialize(self)
	return { unpack(self, 1, self.n) }
end

function List:constructor(generic)
	assert(class[generic], "List constructor expected class")
	local classname = "List<"..generic:name()..">"
	if class[classname] then
		return class[classname]
	end

	local prototype = {
		generic = generic,
	}
	for method, func in pairs(methods) do
		prototype[method] = func
	end
	local newclass = class(classname) : prototype(prototype) : noautocast()
	newclass.__serialize = serialize

	newclass.constructor = self.object_constructor
	return newclass
end

function List:object_constructor(...)
	-- Constructor
	-- List(Object)({1, 2, 3, true, false})
	-- List(Object)(1, 2, true, {}, function() end)

	local args
	local nargs = select('#', ...)
	if nargs > 1 then
		args = { n = select('#', ...), ... }
	elseif nargs == 1 then
		if type(...) == 'table' then
			args = ...
		else
			error("Usage: List(<class>)({...}) got " .. type(...))
		end
	else
		self.n = 0
		return self
	end

	local n = 0
	for i = 1, args.n or #args do
		table.insert(self, self.generic(args[i]))
		n = n + 1
	end

	self.n = n
	return self
end

function methods:length()
	return self.n
end

function methods:dropnils()
	local last = 0
	for j = 1, self:length() do
		if self[j] ~= nil then
			last = last + 1
			self[last] = self[j]
		end
	end
	for j = last+1, self:length() do
		self[j] = nil
	end
	self.n = last
end

function methods:push(...)
	local args = { n = select('#', ...), ... }
	for i = 1, args.n do
		self.n = self.n+1
		table.insert(self, self.generic(args[i]))
	end
end

function methods:pop()
	if self.n > 0 then self.n = self.n - 1 end
	return table.remove(self)
end

function methods:shift()
	if self.n > 0 then self.n = self.n - 1 end
	return table.remove(self, 1)
end

function methods:unshift(...)
	local args = { n = select('#', ...), ... }
	for i = 1, args.n do
		self.n = self.n + 1
		table.insert(self, i, self.generic(args[i]))
	end
end

function methods:take_from(list)
	assert(list:isa(self:who()), list:who() .. " is not " .. self:who())

	for i = 1, list:length() do
		self.n = self.n + 1
		table.insert(self, self.generic(list[i]))
	end

	return self
end

function methods:clear()
	for i = 1, self:length() do
		self[i] = nil
	end
	self.n = 0
end

function methods:each(callable, ...)
	for i = 1, self:length() do
		callable(self[i], ...)
	end
end

function methods:has(item)
	for i = 1, self:length() do
		if self[i] == item then
			return true
		end
	end
	return false
end

function methods:pairs()
	return ipairs(self)
end

return List