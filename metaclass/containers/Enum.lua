local class = require 'metaclass'


local Enum = class "Enum" : prototype {
	unique = {}
}

function Enum:constructor(args)
	for key, value in pairs(args) do
		local index
		if type(key) == 'number' then
			index = value
		else
			index = key
		end

		if self.unique[index] ~= nil then
			error(("Key %s already present"):format(index))
		end

		self.unique[index] = value
	end
end

function Enum:__call(value)
	local res = self.unique[value]
	if res == nil then
		error(("Value %s not defined in Enum"):format(value))
	end
	return res
end

return Enum