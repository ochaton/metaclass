local class = require 'metaclass'

local LinkedListNode = class "LinkedListNode"
function LinkedListNode:constructor(data, prev, next)
	self.data = data
	self.prev = prev or self
	self.next = next or self
end

local LinkedList = class "LinkedList"

function LinkedList:_insert(item)
	local node = LinkedListNode(item, self.tail, self.head)
	if not self.tail then
		self.head, self.tail = node, node
	else
		self.head.prev = node
		self.tail.next = node
	end
	return node
end

function LinkedList:empty()
	return self.head == nil
end

function LinkedList:push(item)
	self.tail = self:_insert(item)
	return self.tail
end

function LinkedList:unshift(item)
	self.head = self:_insert(item)
	return self.head
end

function LinkedList:remove(e)
	local H, T = self.head, self.tail

	local L, R = e.prev, e.next
	e.prev, e.next = nil, nil

	if H == e then self.head = R end
	if T == e then self.tail = L end

	if L == e --[[or R == e]] then
		self.head, self.tail = nil, nil
		return e
	end

	L.next, R.prev = R, L
	return e
end

function LinkedList:pop()
	local T = self.tail
	if not T then return end

	return self:remove(T).data
end

function LinkedList:shift()
	local H = self.head
	if not H then return end

	return self:remove(H).data
end

function LinkedList:generator(cur)
	local H = self.head
	if not H then return nil end
	if cur == nil then
		return H.next, H.data
	end
	if cur == H then return nil end
	return cur.next, cur.data
end

function LinkedList:pairs()
	return self.generator, self, nil
end

return LinkedList