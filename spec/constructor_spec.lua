local class = require 'metaclass'

describe("Creating class", function()
	before_each(function()
		if class["A"] then
			class:delete "A"
		end
	end)

	it("should create class", function()
		local A = class "A"
		assert.are.same(A:name(), "A")
		assert.are.same(A, class.A)
		assert.are.same(A, class[A])
	end)

	it("should not create second class with the same name", function()
		class "A"
		assert.has_error(function() class "A" end)
	end)

	it("should instantiate object", function()
		local A = class "A"
		local a1 = A()

		assert.are.same(a1:who(), "A")
		assert.is_true(a1:isa(A))
		assert.is_true(a1:isa(class.Object)) -- every object isa Object

		assert.has_error(function() a1:isa("B") end) -- class B is undefined
	end)

	it("should call constructor", function()
		local A = class "A"

		local called_with
		function A.constructor(...) called_with = {...} end

		local a = A("hello", "there")
		assert.are.same({a, "hello", "there"}, called_with)

		local Sum = class "Sum" : prototype {
			base = 0
		}

		function Sum:constructor(...)
			for i = 1, select('#', ...) do
				self.base = self.base + select(i, ...)
			end
			return self
		end

		assert.are.same(Sum(1, 2, 3).base, 6)
		assert.are.same(Sum().base, 0)

		assert.is_false(Sum():isa(A))
		assert.is_false(Sum():isa "A")

		class:delete "Sum"
	end)

	it("should inherit parent methods", function()
		local Creature = class "Creature"

		function Creature:speak()
			return ("I'm %s"):format(self:who())
		end

		local Dog = class "Dog" : inherits "Creature"
		assert.are.same(Dog():speak(), "I'm Dog")

		local Human = class "Human" : inherits "Creature"

		function Human:speak()
			return ("My name is %s"):format(self:who())
		end

		assert.are.same(Human():speak(), "My name is Human")
	end)

	it("should copy object", function()
		local A = class "A"

		local data = { key = "value" }
		assert.True(A(data) == data)

		local copy = A(A(data))
		assert.True(copy == data)

		function A:copy()
			return A({ key = self.key })
		end

		assert.falsy(A(A(data)) == data)
	end)

	it("should fail inherit more than one parent", function()
		class "A"
		class "B"
		assert.has_error(function() class "C" : inherits("A", "B") end)
	end)

	it("Should add metamethods", function()
		local Vector2D = class "Vector2D" : prototype {
			x = 0,
			y = 0,
		}

		function Vector2D.__add(v1, v2)
			return Vector2D { x = v1.x + v2.x, y = v1.y + v2.y }
		end

		local v1 = Vector2D()
		local v2 = Vector2D{ x = 1, y = 2 }

		assert.are.same(v1+v2, { x = 1, y = 2 })
		assert.are.same(v1, { x = 0, y = 0 })
		assert.are.same(v2, { x = 1, y = 2 })
	end)
end)
