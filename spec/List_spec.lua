local class = require 'metaclass'
local List = require 'metaclass.containers.List'

describe("Creating simple List", function()
	it("should work for Object class", function()
		assert.truthy(List(class.Object))
	end)

	it("should create list from Objects", function()
		local list = List(class.Object){ "a", "b", "c", "d", "e" }
		assert.are.same(list:length(), 5)
		assert.are.same(list.n, 5)
		assert.are.same(#list, 5)
	end)

	it("should correcly remove nil from the middle of the list", function()
		local list = List(class.Object){}
		list:push "a"
		list:push "b"
		list:push "c"

		list[2] = nil
		list:dropnils()

		assert.are.same(list, { "a", "c", n = 2 })
	end)

	it("should corrctly shift items to the beginning", function()
		local list = List(class.Object){}
		list:push "a"
		list:push "b"
		list:push "c"

		list[1] = nil
		list[2] = nil

		list:dropnils()
		assert.are.same(list, { "c", n = 1 })
	end)
end)
