---@classmod metaclass
---@author Vladislav Grubov
---@usage
---local class = require 'metaclass'
---local Pet = class "Pet"
---function Pet:constructor()
---    print("name:", self.name)
---end
---Pet { name = 'bird' } --> stdout: name: bird
---
---local Dog = class "Dog" : inherits(Pet) : prototype {
---    type = 'dog',
---}
---function Dog:speak()
---    print("bark:", self.name)
---end
---Dog { name = 'Lucky' }:speak() --> stdout: bark: Lucky
---
---local Cat = class "Cat" : inherits "Pet" : prototype {
---    type = 'cat',
---}
---
---function Cat:speak()
---    print("meow")
---end
---Cat { name = 'Kitty' }:speak() --> stdout: meow
---return Pet
local ClassBuilder = {}
ClassBuilder.__metatable = {}
setmetatable(ClassBuilder, ClassBuilder.__metatable)

--- Base table and metatable of each class
---@table Class
---@field methods table<string,function> methods and prototype of the class
---@field __name string name of the class
---@field __autocast boolean (default:true) flag to implement generic Classes
local Class = {}
Class.__metatable = {}
setmetatable(Class, Class.__metatable)

local raw = {
	constructor = true,
	parent      = true,
	copy        = true,
}

local metamethods = {
	__eq = true, __lt = true, __le = true,
	__len = true,
	__unm = true, __add = true, __sub = true, __mul = true, __div = true, __mod = true, __pow = true, __idiv = true,
	__band = true, __bxor = true, __bnot = true, __bshl = true, __bshr = true,
	__mode = true,
	__pairs = true, __ipairs = true,
	__concat = true, __tostring = true,
	__call = true,
	__serialize = true,
}

function Class.__metatable.__call(_, classname)
	local mt = {
		__name  = classname,
		__index = Class,
		__newindex = function (class, index, value)
			if raw[index] then
				rawset(class, index, value)
			elseif metamethods[index] then
				class.__object_metatable[index] = value
			else
				class.methods[index] = value
			end
		end,
		__serialize = function(class)
			return "Class:"..class.__name
		end,
		__call  = Class.instance_object,
	}
	-- new class:
	local class = {
		methods = {},
		__autocast = true,
		__name  = classname,
	}
	class.__object_metatable = {
		__index = class.methods,
		__name  = class.__name,
		class   = class,
	}
	return setmetatable(class, mt)
end

local function tail_constructor(o, ...)
	if select('#', ...) == 0 then
		return o
	else
		return ...
	end
end

---Instantiate object
---Creates new object of a Class
---
---Calls Class:copy if defined
---
---Calls Class:constructor if defined
---@param ... any variables to construct new object of Class
---@return Object
function Class:instance_object(...)
	local object
	if select('#', ...) == 1 and type(...) == 'table' then
		local arg = ...
		if arg.isa == ClassBuilder.Object.methods.isa and arg:isa(self) then
			-- if arg is already the class
			if self.copy then
				return (assert(self.copy(arg), "copy constructor must return object"))
			elseif self.__autocast then
				object = arg
			end
		elseif self.__autocast then
			object = arg
		end
	end

	object = setmetatable(object or {}, self.__object_metatable)

	if type(self.constructor) == 'function' then
		return tail_constructor(object, self.constructor(object, ...))
	end
	return object
end

---Instantiates new object of the Class
---@function Class:new
---@see Class:instance_object
---@treturn Object object
Class.new = Class.instance_object

---Sets parent class
---
---
---can have only one parent
---@param Class
---@return Class
function Class:inherits(...)
	local numargs = select("#", ...)
	if numargs ~= 1 then
		error("Expected exactly 1 class to inherit", 2)
	end

	local parent = ClassBuilder[...]

	if not parent then
		error(string.format("Parent class '%s' not found", ...), 2)
	end

	self.parent = parent
	self.__object_metatable.parent = parent
	setmetatable(self.methods, {
		__index = parent.methods,
		__name  = self.__name .. ".Methods",
	})

	for method in pairs(metamethods) do
		if parent.__object_metatable[method] then
			self.__object_metatable[method] = parent.__object_metatable[method]
		end
	end

	self.constructor = parent.constructor
	return self
end

---Getter of class name
---@treturn string name
function Class:name()
	return self.__name
end

---Disables autocast of Class:constructor
---usefull for generic Classes
---@return Class
function Class:noautocast()
	self.__autocast = false
	return self
end

---Method of Class to setup shared table for all instances of the Class
---@tparam table proto
---@treturn Class class
function Class:prototype(proto)
	assert(type(proto) == 'table', "Prototype must be table")
	for k,v in pairs(proto) do
		if rawget(self.methods, k) then
			error(("Name %s already exists in class"):format(k), 2)
		end
		self.methods[k] = v
	end
	return self
end

local defined_at = {}

function ClassBuilder.__metatable.__call(_, name)
	if ClassBuilder[name] then
		error(("Class %s is already defined at %s"):format(name, defined_at[name]), 2)
	end

	local class = Class(name)
	local caller = debug.getinfo(2)
	defined_at[name] = caller.short_src .. ":" .. caller.currentline
	if ClassBuilder.Object then
		class:inherits(ClassBuilder.Object)
	end

	ClassBuilder[name] = class
	ClassBuilder[class] = class
	return class
end

---allows to delete class from registry
---@function metaclass:delete
---@param class_or_name string|Class
---@return Class
function ClassBuilder:delete(class_or_name)
	local class = assert(self[class_or_name], "class does not exist")
	self[class] = nil
	self[class:name()] = nil
	return class
end

---Object is the base class for all classes
---@table Object
local Object = ClassBuilder("Object")

--- isa checks if given class is a child of given class
---@tparam Class|string someClass
---@treturn boolean
function Object:isa(someClass)
	assert(someClass, ":isa expects class or classname")
	if type(someClass) == "string" then
		if not ClassBuilder[someClass] then
			error(string.format("Class %s not found", someClass), 2)
		end
		someClass = ClassBuilder[someClass]
	end

	local mt = getmetatable(self)
	if type(mt) ~= 'table' then return false end
	local class = mt.class

	while class do
		if class == someClass then
			return true
		end
		class = class.parent
	end
	return false
end

---who returns name of the class of object
---@treturn string name of the class
function Object:who()
	local mt = getmetatable(self)
	if type(mt) ~= 'table' then return false end

	local class = mt.class
	if not class then return false end
	return class:name()
end

---super returns parent class
---@treturn Class parent
function Object:super()
	return ClassBuilder[self:who()].parent
end

---Object constructor returns given arguments to the Object class
---@return ...
function Object:constructor(...)
	return select('#', ...) == 0 and self or ...
end

---Object:copy returns self object
---@treturn Object self
function Object:copy() return self end

return ClassBuilder
